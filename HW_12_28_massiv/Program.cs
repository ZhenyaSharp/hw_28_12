﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_12_28_massiv
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            int[] mas1;
            int temp;
            int count = 0;

            Console.WriteLine("Введите размер массива: ");
            n = int.Parse(Console.ReadLine());

            mas1 = new int[n];

            Console.WriteLine("Заполните массив с повторением некоторых элементов");

            for (int i = 0; i < n; i++)
            {
                mas1[i] = int.Parse(Console.ReadLine());
            }
            Console.Clear();


            for (int i = 0; i < n; i++)
            {
                Console.Write(mas1[i] + " ");
            }
            Console.WriteLine();

            for (int i = 0; i < n; i++)
            {
                temp = mas1[i];

                for (int j = i + 1; j < n; j++)
                {
                    if (temp == mas1[j])
                    {
                        Console.Write($"повторяется {i} и {j} ");
                        count++;
                    }
                }
            }

            for (int i = 0; i < n; i++)
            {
                temp = mas1[i];

                for (int j = i + 1; j < n; j++)
                {
                    if (temp == mas1[j])
                    {
                        for (int k = j; k < n-1; k++)
                            mas1[k] = mas1[k + 1];
                    }
                }
            }
            Console.WriteLine();
            for (int i = 0; i < n-count; i++)
            {
                Console.Write(mas1[i]);
            }
            Console.ReadKey();


        }
    }
}
